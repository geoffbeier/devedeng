# Copyright 2019 (C) Raster Software Vigo (Sergio Costas)
#
# This file is part of DeVeDe-NG
#
# DeVeDe-NG is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# DeVeDe-NG is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

from gi.repository import Gtk, GObject
import os

import devedeng.configuration_data
import devedeng.interface_manager

class separator(devedeng.interface_manager.interface_manager):

    __gsignals__ = {'name_changed': (GObject.SIGNAL_RUN_FIRST, None, (str,)), 'page_jump_changed': (
        GObject.SIGNAL_RUN_FIRST, None, (bool,))}

    def __init__(self):
        self.element_type = "separator"
        devedeng.interface_manager.interface_manager.__init__(self)
        self.add_toggle("page_jump", False)
        self.add_text("separator_name", _("Separator"))

    def set_name(self, new_name):
        self.separator_name = new_name
        self.emit('name_changed', self.separator_name)

    def set_page_jump(self, page_jump):
        self.page_jump = page_jump
        self.emit('page_jump_changed', self.page_jump)

    def delete_element(self):
        return

    def store_element(self):
        data = self.serialize()
        data["element_type"] = "separator"
        return data

    def restore_element(self, data):
        self.unserialize(data)